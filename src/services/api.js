import axios from "axios";

const searchBreed = (value) => {
    return new Promise((resolve, reject) => {
        axios.get(`${process.env.REACT_APP_API}/images/search?page=1&limit=10&breed_id=${value}`)
        .then(breeds => {
            resolve(breeds.data.map(breed => breed));
        }, err => {
            reject({ err, message: `Apologies butwe could not load new cats for you at this time! Miau!` })
        })
    })
}

const getBreeds = () => {
    return new Promise((resolve, reject) => {
        axios.get(`${process.env.REACT_APP_API}/breeds`)
        .then(breeds => {
            resolve(breeds.data.map(breed => ({ id: breed.id, name: breed.name })))
        }, err => {
            reject({ err, message: `Apologies butwe could not load new cats for you at this time! Miau!` })
        })
    }) 
}

const getBreed = (id) => {
    return new Promise((resolve, reject) => {
        axios.get(`${process.env.REACT_APP_API}/images/${id}`)
        .then(breed => {
            const cat = {...breed.data.breeds[0], url: breed.data.url }
            resolve(cat)
        }, err => {
            reject({ err, message: `Apologies butwe could not load new cats for you at this time! Miau!` })
        })
    })
}

export { searchBreed, getBreeds, getBreed };
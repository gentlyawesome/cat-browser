import { Container, Navbar } from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import styles from "./App.module.css"
import { Fragment } from "react";
import Routes from "./Routes";

function App() {
  return (
    <Fragment>
      <Navbar bg="light" className={styles.navHeader}>
      <Container>
        <Navbar.Brand href="/">
          <img alt="" src="./logo.png" width="30" height="30" className="d-inline-block align-top" />{' '}
          Cat Browser
        </Navbar.Brand>
      </Container>
      </Navbar>
      <Routes />
    </Fragment>
  );
}

export default App;

import { useEffect, useState } from "react";
import { Col, Container, Form, Row } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import CatCard from "../../component/catCard";
import { searchBreed, getBreeds }  from '../../services/api'

const Home = () => {
    const history = useHistory();
    const query = new URLSearchParams(window.location.search) 
    const breedQuery = query.get('breed') 

    const [breeds, setBreeds ] = useState();
    const [selectedBreed, setSelectedBreed ] = useState();
    const [selected, setSelected ] = useState();


    const handleChange = async (value) => {
        setSelectedBreed(await searchBreed(value))
    }

    const handleViewBreed = (breed) => {
        history.push(`/${breed}`)
    }

    useEffect(() => {
        const setQuery = async () => {
            if(breedQuery){
                setSelectedBreed(await searchBreed(breedQuery))
            }

            setBreeds(await getBreeds())
        }

       setQuery();
    }, [])

    useEffect(() => {
        searchBreed(selected)
    }, [selected])


    return (
        <Container>
            <Row>
                <Col xs={12} sm={3}>
                {breeds && 
                  <Form.Select aria-label="Select Breed" size="lg" defaultValue={selected} onChange={(event) => handleChange(event.target.value)}>
                      <option>Select Breed</option>
                      {breeds.map((breed, index) => (
                        <option key={index} value={breed.id}>{breed.name}</option>
                      ))}
                  </Form.Select>
                }
                </Col>
                <Col>
                    <Row>
                        { selectedBreed && selectedBreed.map((breed, index) => <CatCard key={index} item={breed} viewBreed={handleViewBreed} />) }
                    </Row>
                </Col>
            </Row>
        </Container>
    )
}

export default Home;
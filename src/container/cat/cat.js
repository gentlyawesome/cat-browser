import { useEffect, useState } from "react";
import { Card, Col, Container, Row, Button, CardColumns } from "react-bootstrap";
import { useHistory, useParams } from "react-router-dom";
import { getBreed } from "../../services/api";


const Cat = () => {
    const history = useHistory();
    const [breed, setBreed] = useState();
    const { id } = useParams();

    const handleBack = (value) => {
        history.push(`/?breed=${value}`)
    }

    const handleGetBreed = async (id) => {
      setBreed(await getBreed(id))
    }

    useEffect(() => {
        handleGetBreed(id)
    }, [])

    return (
        <Container>
            <Row>
                <Col xs={12} md={8} lg={8}>
                {breed && 
                    <CardColumns>
                        <Card>
                        <Card.Body>
                            <Button variant="primary" onClick={() => handleBack(breed.id)}>Back</Button>
                        </Card.Body>
                        <Card.Img variant="top" src={breed.url} />
                        <Card.Body>
                            <Card.Title>{ breed.name }</Card.Title>
                            <Card.Text>{ breed.description }</Card.Text>
                        </Card.Body>
                        </Card>
                    </CardColumns>
                }
                </Col>
            </Row>
        </Container>
    )
}

export default Cat;
import { Fragment } from "react";
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";

import Cat from "./container/cat/cat";
import Home from "./container/home/home";

const Routes = () => {
    return (
    <Fragment>
      <Router>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/:id" component={Cat} />
          </Switch>
      </Router>
    </Fragment>
    )
}

export default Routes;
import { Card, Button, Col } from "react-bootstrap";

const CatCard = (props) => {
    const { url, id } = props.item;
    return (
        <Col className={`g-4`}>
            <Card style={{ width: '18rem' }}>
                <Card.Img variant="top" src={url} />
                <Card.Body>
                    <Button variant="primary" onClick={() => props.viewBreed(id)} >View Details</Button>
                </Card.Body>
            </Card>
        </Col>
    )
}

export default CatCard;
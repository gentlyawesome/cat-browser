require('dotenv').config()

const express = require('express')
const morgan = require('morgan');
const app = express();

app.use(morgan('dev'));
app.use(express.urlencoded({ extended: true })) // to support URL-encoded bodies
app.use(express.json({ limit: '50mb' }));       // to support JSON-encoded bodies

app.use(express.static(process.cwd() + '/build'));
app.use(express.static(process.cwd() + '/public'));
app.use('/*/static', express.static(process.cwd() + '/build/static'));

app.get('/*', function (req, res, next) {
  res.sendFile(process.cwd() + '/build/index.html', function (err) {
    if (err) {
      res.status(500).send(err);
    }
  })
});

app.listen(process.env.PORT, () => {
  console.log(`Listening on port ${process.env.PORT}`)
});